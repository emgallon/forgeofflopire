package infrastructure;

import infrastructure.events.Event;
import domain.entities.buildings.Building;
import domain.entities.buildings.LivingBuilding;
import domain.entities.buildings.WorkingBuilding;
import domain.entities.human.Human;
import domain.entities.resources.ResourceType;

import java.util.*;

/**
 * Toute la logique de stockage des données est stockée dans l'infrastructure
 */
public class GameInfrastructure {

    //instance unique
    private static GameInfrastructure gameInfrastructureInstance;
    private ArrayList<Building> buildings; // contient les batiments du jeu
    private HashSet<Event> gameEvents; // contient les différents évènement non fini

    private HashMap<ResourceType,Integer> resources; //quantité de resources disponibles

    public void addBuilding(Building building) {
        this.buildings.add(building);
    }
    public ArrayList<Building> getBuildings() {
        return this.buildings;
    }

    public ArrayList<Human> getHumans() {
        ArrayList<Human> humans = new ArrayList<>();
        for(Building b: buildings) {
            if(b instanceof LivingBuilding) {
                LivingBuilding lb = (LivingBuilding) b;
                humans.addAll(lb.getResidents());
            }
            if(b instanceof WorkingBuilding) {
                WorkingBuilding wb = (WorkingBuilding) b;
                humans.addAll(wb.getWorkers());
            }
        }
        return humans;
    }

    public static GameInfrastructure getGameInfrastructureInstance() {
        if(gameInfrastructureInstance == null)
            gameInfrastructureInstance = new GameInfrastructure();
        return gameInfrastructureInstance;
    }
    protected GameInfrastructure() {
        this.resources = new HashMap<>();
        for(ResourceType type : ResourceType.values()) {
            this.resources.put(type,0);
        }
        this.gameEvents = new HashSet<>();
        this.buildings = new ArrayList<>();
    }

    public void addGameEvent(Event event) {
        this.gameEvents.add(event);
    }
    public HashSet<Event> getGameEvent() {
        return this.gameEvents;
    }

    public void removeGameEvent(ArrayList<Event> event) {
        this.gameEvents.removeAll(event);
    }

    //GESTION DES RESSOURCES
    public int getResource(ResourceType resourceType) {
        return resources.get(resourceType).intValue();
    }

    public void addResource(ResourceType resourceType, int quantity) {
        resources.put(resourceType, (resources.get(resourceType).intValue()+quantity));
    }

    public void payResource(Building building) {
        removeResource(ResourceType.GOLD,building.getCost());
        HashMap<ResourceType,Integer> resourcesList = building.getBuildingMaterials().getMaterialsList();
        for(ResourceType resourceType : resourcesList.keySet()) {
            removeResource(resourceType, resourcesList.get(resourceType));
        }
    }

    public boolean removeResource(ResourceType resourceType, int quantity) {
        if(quantity > resources.get(resourceType)) {
            return false;
        }
        resources.put(resourceType, (resources.get(resourceType)-quantity));
        return true;
    }

}
