package infrastructure.events;


import java.lang.reflect.Method;
/**
 * Classe permettant de créer un évènement. Cet évènement, une fois envoyé dans
 * l'infrastructure sera executé par le manager au bout de x ticks.
 * L'évènement execute donc la méthode qui lui est passé en argument en utilisant la réflexion.
 * Dans le projet, il est uniquement utilisé pour la création des batiments étant donné que c'est
 * le seul évènement qui se déclenche au bout de X ticks.
 */
public class Event {
    private int tickNumber;
    private String eventDescription;
    private Method eventAccomplish;

    private Object object;

    private Object[] parameters;

    public Event(int tickNumber, String eventDescription, Method eventAccomplish,Object object, Object[] parameters) {
        this.tickNumber = tickNumber;
        this.eventDescription = eventDescription;
        this.eventAccomplish = eventAccomplish;
        this.object = object;
        this.parameters = parameters;
    }

    public String getEventDescriptionStillRunning() {
        return eventDescription+" en cours, "+tickNumber+" ticks restants.";
    }

    public String getEventDescriptionFinished() {
        return eventDescription+" fini !";
    }

    public Object getObject() {
        return this.object;
    }

    public Object[] getParameters() {
        return this.parameters;
    }

    public void decrementTick() {
        this.tickNumber--;
    }

    public int getTickNumber() {
        return tickNumber;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public Method eventAccomplish() {
        return eventAccomplish;
    }
}
