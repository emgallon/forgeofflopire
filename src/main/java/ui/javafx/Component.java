package ui.javafx;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * Un component est un objet qui sera affiché à l'écran plus tard sous forme d'un carré de couleur.
 * En cliquant dessus, les informations disponibles se mettront à jour pour correspondre à l'objet.
 * Chaque building étant de Component
 */
public class Component {

    private Rectangle rectangle;
    private int startPositionX;
    private int startPositionY;

    public Component(int size, int startPositionX, int startPositionY, Color color) {
        this.startPositionX = startPositionX;
        this.startPositionY = startPositionY;
        rectangle = new Rectangle(startPositionX, startPositionY, size, size);
        rectangle.setFill(color);
        rectangle.setStroke(Color.BLACK);
    }

    public Rectangle getRectangle() {
        return rectangle;
    }

    public int getStartPositionX() {
        return startPositionX;
    }

    public int getStartPositionY() {
        return startPositionY;
    }
}