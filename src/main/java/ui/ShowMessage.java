package ui;

import javafx.application.Platform;
import application.game.Controller;

public class ShowMessage {

    public static void showMessage(String message, Controller controller) {
        if(controller == null)
            System.out.println(message);
        else {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    controller.showMessage(message);
                }
            });
        }

    }
}
