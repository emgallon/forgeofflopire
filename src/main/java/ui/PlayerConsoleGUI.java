package ui;

import application.game.PlayerController;

import java.util.Scanner;

/**
 * Objet qui permet de gérer l'interface ligne de commande
 */
public class PlayerConsoleGUI {


    private PlayerController playerController;

    public PlayerConsoleGUI(PlayerController playerController) {
        this.playerController = playerController;
    }
    public void playerChoices() {
        while(true) {
            ShowMessage.showMessage("1 - Reprendre la partie",null);
            ShowMessage.showMessage("2 - Acheter un batiment",null);
            ShowMessage.showMessage("3 - Retirer un batiment",null);
            ShowMessage.showMessage("4 - Ajouter un habitant",null);
            ShowMessage.showMessage("5 - Ajouter un travailleur",null);
            ShowMessage.showMessage("6 - Retirer un habitant",null);
            ShowMessage.showMessage("7 - Retirer un travailleur",null);
            ShowMessage.showMessage("q - Quitter",null);

            Scanner reader = new Scanner(System.in);
            char selection = reader.next().charAt(0);
            if (selection == '1') {
                playerController.resumeGame();
                return;
            } else if (selection == '2') {
                playerController.buildingSelectionChoice();
            } else if (selection == '3') {
                playerController.removeBuildingChoice();
            } else if (selection == '4') {
                playerController.addHumanChoice(true);
            } else if (selection == '5') {
                playerController.addHumanChoice(false);
            } else if(selection == '6'){
                playerController.removeHumanChoice(true);
            } else if(selection == '7') {
                playerController.removeHumanChoice(false);
            } else if(selection == 'q') {
                System.exit(0);
            }else {
                return;
            }
        }
    }
}
