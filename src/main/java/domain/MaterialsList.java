package domain;

import domain.entities.resources.ResourceType;

import java.util.HashMap;

/**
 * Objet permettant de stocker une liste de matériaux. Utile pour savoir ce que produit/consomme/coute un batiment.
 */
public class MaterialsList {
    private HashMap<ResourceType, Integer> materials;

    public MaterialsList() {
        this.materials = new HashMap<>();
    }

    public HashMap<ResourceType, Integer> getMaterialsList() {
        return materials;
    }

    public void put(ResourceType resourceType, int quantity) {
        materials.put(resourceType,quantity);
    }
}

