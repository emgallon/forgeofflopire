package domain.entities.resources;


/**
 * Le système de ressources est basé sur des type et la gestion des quantités se trouve dans l'infrastructure.
 * Cela évite de remplir la mémoire d'objet de ressource. (étant donné qu'il peut y en avoir des milliers).
 */
public enum ResourceType {
    //Ressources disponibles
    CEMENT,COAL,FOOD,GOLD,IRON,LUMBER,STEEL,STONE,TOOLS,WOOD;

    /**
     * Retourne une String correspondant au nom de la ressource en fonction d'un type passé
     * @param resourceType
     * @return
     */
    public static String fromResource(ResourceType resourceType) {
        if(resourceType == CEMENT) {
            return "Cement";
        } else if(resourceType == COAL) {
            return "Coal";
        }else if(resourceType == FOOD) {
            return "Food";
        }else if(resourceType == GOLD) {
            return "Gold";
        }else if(resourceType == IRON) {
            return "Iron";
        }else if(resourceType == LUMBER) {
            return "Lumber";
        }else if(resourceType == STEEL) {
            return "Steel";
        }else if(resourceType == STONE) {
            return "Stone";
        }else if(resourceType == TOOLS) {
            return "Tools";
        }else if(resourceType == WOOD) {
            return "Wood";
        }
        return "";
    }

    /**
     * Retourne depuis une valeur passé en paramètres, le type correspondant.
     * @param value
     * @return
     */
    public static ResourceType fromString(String value) {
        if(value.equalsIgnoreCase("cement")) {
            return CEMENT;
        } else if(value.equalsIgnoreCase("coal")) {
            return COAL;
        }else if(value.equalsIgnoreCase("food")) {
            return FOOD;
        }else if(value.equalsIgnoreCase("gold")) {
            return GOLD;
        }else if(value.equalsIgnoreCase("iron")) {
            return IRON;
        }else if(value.equalsIgnoreCase("lumber")) {
            return LUMBER;
        }else if(value.equalsIgnoreCase("steel")) {
            return STEEL;
        }else if(value.equalsIgnoreCase("stone")) {
            return STONE;
        }else if(value.equalsIgnoreCase("tool")) {
            return TOOLS;
        }else if(value.equalsIgnoreCase("wood")) {
            return WOOD;
        } else {
            return null;
        }
    }
}
