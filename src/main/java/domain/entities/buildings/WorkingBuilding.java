package domain.entities.buildings;

import domain.entities.human.Resident;
import domain.entities.human.Worker;

import java.util.ArrayList;

/**
 * Interface permettant de dire qu'un batiment abrite des travailleurs
 */
public interface WorkingBuilding {
    void addWorker(Worker worker);
    ArrayList<Worker> getWorkers();
}
