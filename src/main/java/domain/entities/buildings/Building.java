package domain.entities.buildings;

import domain.MaterialsList;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import ui.javafx.Component;

import java.util.Random;
import java.util.UUID;

public abstract class Building extends Component {

    protected int initialResidentsNumber;
    protected int initialWorkersNumber;
    protected int cost;
    protected MaterialsList buildingMaterials;
    protected MaterialsList consumption;
    protected MaterialsList production;
    private int tool;
    protected int buildingTime;
    protected UUID id;

    /**
     *
     * @param initialResidentsNumber Nombre initial de résidents
     * @param initialWorkersNumber Nombre initial de travailleurs
     * @param cost Cout du batiment
     * @param buildingMaterials Liste des matériaux nécessaire à la construction de ce batiment
     * @param consumption Liste de ce que consomme le batiment par ouvrier
     * @param production Liste de ce que produit le batiment par ouvrier
     * @param buildingTime Temps de construction du batiment
     * @param color Couleur affiché sur l'UI du batiment
     */
    public Building(
            int initialResidentsNumber,
            int initialWorkersNumber,
            int cost,
            MaterialsList buildingMaterials,
            MaterialsList consumption,
            MaterialsList production,
            int buildingTime,
            Color color
    ) {
        super(20, (int) ((double)Math.random()*400),(int)((double)Math.random()*200), color);
        this.id = UUID.randomUUID();
        this.initialResidentsNumber = initialResidentsNumber;
        this.initialWorkersNumber = initialWorkersNumber;
        this.cost = cost;
        this.buildingMaterials = buildingMaterials;
        this.consumption = consumption;
        this.production = production;
        this.buildingTime = buildingTime;
    }
    public int getInitialResidentsNumber() {
        return initialResidentsNumber;
    }

    public int getInitialWorkersNumber() {
        return initialWorkersNumber;
    }

    public int getCost() {
        return cost;
    }

    public abstract String getName();

    public MaterialsList getBuildingMaterials() {
        return buildingMaterials;
    }

    public MaterialsList getConsumption() {
        return consumption;
    }

    public MaterialsList getProduction() {
        return production;
    }

    public int getBuildingTime() {
        return buildingTime;
    }

    public UUID getId() {
        return id;
    }

    public abstract String toString();

}
