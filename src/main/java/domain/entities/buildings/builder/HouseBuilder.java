package domain.entities.buildings.builder;

import domain.entities.buildings.House;
import javafx.scene.paint.Color;

public class HouseBuilder extends BuildingBuilder {

    @Override
    public House build() {
        House building = new House(
                residentsNumber, workersNumber, cost, buildingMaterials, consumption, production, buildingTime, Color.BLUEVIOLET
        );
        return building;
    }
}
