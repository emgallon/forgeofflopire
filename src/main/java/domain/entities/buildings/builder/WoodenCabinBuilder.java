package domain.entities.buildings.builder;

import domain.entities.buildings.WoodenCabin;
import javafx.scene.paint.Color;

public class WoodenCabinBuilder extends BuildingBuilder {
    @Override
    public WoodenCabin build() {
        WoodenCabin building = new WoodenCabin(
                residentsNumber, workersNumber, cost, buildingMaterials, consumption, production, buildingTime, Color.BLACK
        );
        return building;
    }
}