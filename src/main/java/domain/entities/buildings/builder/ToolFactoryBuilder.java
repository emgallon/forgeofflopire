package domain.entities.buildings.builder;

import domain.entities.buildings.ToolFactory;
import javafx.scene.paint.Color;

public class ToolFactoryBuilder extends BuildingBuilder {
    @Override
    public ToolFactory build() {
        ToolFactory building = new ToolFactory(
                residentsNumber, workersNumber, cost, buildingMaterials, consumption, production, buildingTime, Color.DARKORANGE
        );
        return building;
    }
}