package domain.entities.buildings.builder;

import domain.entities.buildings.Building;
import domain.MaterialsList;
import domain.entities.resources.ResourceType;

public abstract class BuildingBuilder {
    protected int residentsNumber;
    protected int workersNumber;
    protected int cost;
    protected MaterialsList buildingMaterials;
    protected MaterialsList consumption;
    protected MaterialsList production;
    protected int buildingTime;

    public void setResidentsNumber(int residentsNumber) {
        this.residentsNumber = residentsNumber;
    }

    public void setWorkersNumber(int workersNumber) {
        this.workersNumber = workersNumber;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public void setBuildingMaterials(MaterialsList buildingMaterials) {
        this.buildingMaterials = buildingMaterials;
    }

    public void setConsumption(MaterialsList consumption) {
        this.consumption = consumption;
    }

    public void setProduction(MaterialsList production) {
        this.production = production;
    }

    public void setBuildingTime(int buildingTime) {
        this.buildingTime = buildingTime;
    }

    public MaterialsList getConsumption() {
        return consumption;
    }

    public int getCost() {
        return cost;
    }

    /**
     * Méthode qui retourne l'objet building correspondant au builder une fois l'objet correctement initialisé.
     * @return
     */
    public abstract Building build();


    /**
     * Retourne une String permettant de visualiser le contenu d'un batiment
     * @return
     */
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("x"+getCost()+" gold, ");

        for(ResourceType resourceType : buildingMaterials.getMaterialsList().keySet()) {
            stringBuilder.append(resourceType+" x"+buildingMaterials.getMaterialsList().get(resourceType)+", ");
        }
        return stringBuilder.toString();
    }

}
