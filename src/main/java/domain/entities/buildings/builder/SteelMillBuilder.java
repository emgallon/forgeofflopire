package domain.entities.buildings.builder;

import domain.entities.buildings.SteelMill;
import javafx.scene.paint.Color;

public class SteelMillBuilder extends BuildingBuilder {
    @Override
    public SteelMill build() {
        SteelMill building = new SteelMill(
                residentsNumber, workersNumber, cost, buildingMaterials, consumption, production, buildingTime, Color.DARKMAGENTA
        );
        return building;
    }
}