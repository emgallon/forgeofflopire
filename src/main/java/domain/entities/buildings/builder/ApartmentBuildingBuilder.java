package domain.entities.buildings.builder;

import domain.entities.buildings.ApartmentBuilding;
import javafx.scene.paint.Color;

public class ApartmentBuildingBuilder extends BuildingBuilder {

    @Override
    public ApartmentBuilding build() {
       ApartmentBuilding building = new ApartmentBuilding(
            residentsNumber, workersNumber, cost, buildingMaterials, consumption, production, buildingTime, Color.color(1,1,0)
       );
       return building;
    }
}
