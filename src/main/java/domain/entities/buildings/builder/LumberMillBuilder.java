package domain.entities.buildings.builder;

import domain.entities.buildings.LumberMill;
import javafx.scene.paint.Color;

public class LumberMillBuilder extends BuildingBuilder {

    @Override
    public LumberMill build() {
        LumberMill building = new LumberMill(
                residentsNumber, workersNumber, cost, buildingMaterials, consumption, production, buildingTime, Color.AZURE
        );
        return building;
    }
}