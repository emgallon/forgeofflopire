package domain.entities.buildings.builder;

import domain.entities.buildings.Quarry;
import javafx.scene.paint.Color;

public class QuarryBuilder extends BuildingBuilder {

    @Override
    public Quarry build() {
        Quarry building = new Quarry(
                residentsNumber, workersNumber, cost, buildingMaterials, consumption, production, buildingTime, Color.BROWN
        );
        return building;
    }
}