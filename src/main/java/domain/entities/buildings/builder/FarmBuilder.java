package domain.entities.buildings.builder;

import domain.entities.buildings.Farm;
import javafx.scene.paint.Color;

public class FarmBuilder extends BuildingBuilder {

    @Override
    public Farm build() {
        Farm building = new Farm(
                residentsNumber, workersNumber, cost, buildingMaterials, consumption, production, buildingTime, Color.BISQUE
        );
        return building;
    }
}
