package domain.entities.buildings.builder;

import domain.entities.buildings.CementPlant;
import javafx.scene.paint.Color;

public class CementPlantBuilder extends BuildingBuilder {

    @Override
    public CementPlant build() {
        CementPlant building = new CementPlant(
                residentsNumber, workersNumber, cost, buildingMaterials, consumption, production, buildingTime, Color.color(0,1,1)
        );
        return building;
    }
}
