package domain.entities.buildings;
import domain.MaterialsList;
import domain.entities.human.Resident;
import javafx.scene.paint.Color;

import java.util.ArrayList;

public class ApartmentBuilding extends Building implements LivingBuilding {

    private ArrayList<Resident> residents;

    public ApartmentBuilding(int residentsNumber, int workersNumber, int cost, MaterialsList buildingMaterials, MaterialsList consumption, MaterialsList production, int buildingTime, Color color) {
        super(residentsNumber, workersNumber, cost, buildingMaterials, consumption, production, buildingTime, color);
        residents = new ArrayList<>();
        for(int i=0; i<initialResidentsNumber; i++) {
            residents.add(new Resident());
        }
    }

    @Override
    public ArrayList<Resident> getResidents() {
        return this.residents;
    }

    @Override
    public String getName() {
        return "ApartmentBuilding";
    }

    @Override
    public String toString() {
        return getName()+" : "+residents.size()+" résidents";
    }

    @Override
    public void addResident(Resident resident) {
        this.residents.add(resident);
    }
}
