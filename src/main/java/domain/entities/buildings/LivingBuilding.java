package domain.entities.buildings;

import domain.entities.human.Resident;

import java.util.ArrayList;

/**
 * Interface permettant de dire qu'un batiment accueille des résidents.
 */
public interface LivingBuilding {
    /**
     * Ajoute le résident à la liste des résident du building
     * @param resident
     */
    void addResident(Resident resident);

    ArrayList<Resident> getResidents();
}
