package domain.entities.buildings;

import domain.MaterialsList;
import domain.entities.human.Worker;
import javafx.scene.paint.Color;

import java.util.ArrayList;

public class ToolFactory extends Building implements WorkingBuilding{
    private ArrayList<Worker> workers;
    public ToolFactory(int residentsNumber, int workersNumber, int cost, MaterialsList buildingMaterials, MaterialsList consumption, MaterialsList production, int buildingTime, Color color) {
        super(residentsNumber, workersNumber, cost, buildingMaterials, consumption, production, buildingTime, color);
        this.workers = new ArrayList<>();
        for(int i=0; i<initialWorkersNumber; i++) {
            workers.add(new Worker());
        }
    }
    @Override
    public ArrayList<Worker> getWorkers() {
        return this.workers;
    }

    public void addWorker(Worker worker) {
        this.workers.add(worker);
    }

    @Override
    public String toString() {
        return getName()+" : "+workers.size()+" travailleurs.";
    }
    @Override
    public String getName() {
        return "ToolFactory";
    }
}
