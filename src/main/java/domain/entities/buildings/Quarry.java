package domain.entities.buildings;


import domain.MaterialsList;
import domain.entities.human.Resident;
import domain.entities.human.Worker;
import javafx.scene.paint.Color;

import java.util.ArrayList;

public class Quarry extends Building implements WorkingBuilding, LivingBuilding{
    private ArrayList<Worker> workers;
    private ArrayList<Resident> residents;
    public Quarry(int residentsNumber, int workersNumber, int cost, MaterialsList buildingMaterials, MaterialsList consumption, MaterialsList production, int buildingTime, Color color) {
        super(residentsNumber, workersNumber, cost, buildingMaterials, consumption, production, buildingTime, color);
        this.workers = new ArrayList<>();
        this.residents = new ArrayList<>();
        for(int i=0; i<initialResidentsNumber; i++) {
            residents.add(new Resident());
        }
        for(int i=0; i<initialWorkersNumber; i++) {
            workers.add(new Worker());
        }
    }
    @Override
    public ArrayList<Resident> getResidents() {
        return this.residents;
    }
    @Override
    public ArrayList<Worker> getWorkers() {
        return this.workers;
    }
    @Override
    public void addResident(Resident resident) {
        this.residents.add(resident);
    }

    @Override
    public String toString() {
        return getName()+" : "+workers.size()+" travailleurs, "+residents.size()+" résidents.";
    }

    public void addWorker(Worker worker) {
        this.workers.add(worker);
    }
    @Override
    public String getName() {
        return "Quarry";
    }
}
