import application.game.Manager;
import infrastructure.GameInfrastructure;


/**
 * Classe Main à executer pour l'interface en ligne de commandes
 */
public class Main {
    public static void main(String[] args) {
        //On crée notre singleton Manager et on lui donne un accès à l'infrastructure (gestion des données).
        //La game loop se lance automatiquement à la création des deux objets.
        Manager.getManagerInstance(GameInfrastructure.getGameInfrastructureInstance());
    }
}
