import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;


/**
 * Classe à executer avec un ./gradlew run
 * à la racine pour lancer l'interface graphique
 * */
public class MainJavaFX extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("interface.fxml"));
        Scene scene = new Scene(fxmlLoader.load());
        stage.setTitle("ForgeOfFlopire");
        stage.setScene(scene);
        stage.show();
    }

    @Override
    public void stop(){
        System.exit(0);
    }

    public static void main(String[] args) {
        launch();
    }
}