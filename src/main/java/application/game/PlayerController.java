package application.game;

import domain.entities.buildings.Building;
import domain.entities.buildings.BuildingType;
import domain.entities.buildings.LivingBuilding;
import domain.entities.buildings.WorkingBuilding;
import domain.entities.buildings.builder.BuildingBuilder;
import domain.entities.human.Resident;
import domain.entities.human.Worker;
import infrastructure.GameInfrastructure;
import ui.ShowMessage;

import java.util.ArrayList;
import java.util.Scanner;

public class PlayerController {
    private Manager manager;

    private GameSettings gameSettings;
    private GameInfrastructure gameInfrastructureInstance;

    private Controller controller;

    public PlayerController(Manager manager, GameSettings gameSettings, GameInfrastructure gameInfrastructureInstance, Controller controller) {
        this.gameSettings = gameSettings;
        this.manager = manager;
        this.controller = controller;
        this.gameInfrastructureInstance = gameInfrastructureInstance;
    }

    public void resumeGame() {
        manager.resumeGame();
    }

    /**
     * Permet de choisir un building à supprimer, méthode utilisé pour l'interface en ligne de commande.
     */
    public void removeBuildingChoice() {
        int i;
        int selection = -1;

        ArrayList<Building> buildings = gameInfrastructureInstance.getBuildings();
        while(selection == -1 || selection >= buildings.size()) {
            i=0;
            for (Building building : gameInfrastructureInstance.getBuildings()) {
                ShowMessage.showMessage("tappez " + i + " pour supprimer ce batiment " + building.toString(),controller);
                i++;
            }
            Scanner reader = new Scanner(System.in);
            selection = Integer.parseInt(reader.next());
        }

        Building buildingToDelete = buildings.get(selection);
    }

    /**
     * Retire le building passé en paramètre. Elle va relocaliser alors tous les habitants / travailleurs.
     * S'il n'y a pas de batiment pouvant accueillir les habitants / travailleurs, ils disparaissent.
     * @param buildingToDelete
     * @return
     */
    public boolean removeBuilding(Building buildingToDelete) {
        ArrayList<Building> buildings = gameInfrastructureInstance.getBuildings();
        if(buildings.size()>1) {
            if (buildingToDelete instanceof LivingBuilding) {
                LivingBuilding livingBuilding = (LivingBuilding) buildingToDelete;
                buildings.remove(livingBuilding);
                relocateResident(livingBuilding.getResidents());
            }
            if (buildingToDelete instanceof WorkingBuilding) {
                WorkingBuilding workingBuilding = (WorkingBuilding) buildingToDelete;
                buildings.remove(workingBuilding);
                relocateWorker(workingBuilding.getWorkers());
            }
            if(controller != null)
                controller.removeBuilding(buildingToDelete);
            ShowMessage.showMessage("Batiment supprimé avec succès", controller);
            return true;
        } else {
            ShowMessage.showMessage("C'est le dernier batiment...",controller);
            return false;
        }
    }

    /**
     * Permet de relocaliser une liste de résidents dans le premier batiment trouvé pouvant les accueillir.
     * @param residents
     */
    public void relocateResident(ArrayList<Resident> residents) {
        for (Building building : gameInfrastructureInstance.getBuildings()) {
            if(building instanceof LivingBuilding) {
                for(Resident resident : residents) {
                    ((LivingBuilding) building).addResident(resident);
                }
                break;
            }
        }
    }

    /**
     * Permet de relocaliser une liste de travailleurs dans le premier batiment trouvé pouvant les accueillir.
     * @param workers
     */
    public void relocateWorker(ArrayList<Worker> workers) {
        for (Building building : gameInfrastructureInstance.getBuildings()) {
            if(building instanceof WorkingBuilding) {
                for(Worker worker : workers) {
                    ((WorkingBuilding) building).addWorker(worker);
                }
                break;
            }
        }
    }

    /**
     * Retire un humain d'un batiment.
     * @param isResident Défini si l'humain a retirer est un travailleur ou un résident.
     * @param building
     */
    public void removeHuman(boolean isResident, Building building) {
        if(isResident) {
            if (manager.removeResident(building)) {
                ShowMessage.showMessage("Vous avez bien retiré 1 habitant.",controller);
            } else {
                ShowMessage.showMessage("Ce batiment n'a pas d'habitants",controller);
            }
        } else {
            if (manager.removeWorker(building)) {
                ShowMessage.showMessage("Vous avez bien retiré 1 travailleur.",controller);
            } else {
                ShowMessage.showMessage("Ce batiment n'a pas de travailleurs",controller);
            }
        }
    }


    /**
     * Méthode permettant via l'interface ligne de commande de choisir de quel batiment retirer 1 travailleur/habitant.
     * @param isResident
     */
    public void removeHumanChoice(boolean isResident) {
        int i;
        int selection = -1;

        ArrayList<Building> buildings = gameInfrastructureInstance.getBuildings();
        while(selection <= -1 || selection >= buildings.size()) {
            i=0;
            for (Building building : gameInfrastructureInstance.getBuildings()) {
                if(isResident) {
                    if(building instanceof LivingBuilding) {
                        ShowMessage.showMessage("tappez " + i + " pour retirer 1 habitant à " + building.toString(),controller);
                    }
                } else {
                    if(building instanceof WorkingBuilding) {
                        ShowMessage.showMessage("tappez " + i + " pour retirer 1 travailleur à " + building.toString(),controller);
                    }
                }
                i++;
            }
            Scanner reader = new Scanner(System.in);
            selection = Integer.parseInt(reader.next());
        }
        removeHuman(isResident,buildings.get(selection));
    }

    /**
     * Méthode permettant via l'interface ligne de commande de choisir de quel batiment ajouter 1 travailleur/habitant.
     * @param isResident
     */
    public void addHumanChoice(boolean isResident) {
        int i;
        int selection = -1;

        ArrayList<Building> buildings = gameInfrastructureInstance.getBuildings();
        while(selection <= -1 || selection >= buildings.size()) {
            i=0;
            for (Building building : gameInfrastructureInstance.getBuildings()) {
                if(isResident) {
                    if(building instanceof LivingBuilding) {
                        ShowMessage.showMessage("tappez " + i + " pour ajouter 1 habitant à " + building.toString(),controller);
                    }
                } else {
                    if(building instanceof WorkingBuilding) {
                        ShowMessage.showMessage("tappez " + i + " pour ajouter 1 travailleur à " + building.toString(),controller);
                    }
                }
                i++;
            }
            Scanner reader = new Scanner(System.in);
            selection = Integer.parseInt(reader.next());
        }
        addHuman(isResident,buildings.get(selection));
    }

    /**
     * Méthode qui à partir d'un batiment retire un habitant/travailleur
     * @param isResident
     * @param building
     */
    public void addHuman(boolean isResident, Building building) {
        if(isResident) {
            if (manager.addResident(building)) {
                ShowMessage.showMessage("Vous avez bien ajouté 1 habitant.",controller);
            } else {
                ShowMessage.showMessage("Ce batiment ne peut contenir d'habitants.",controller);
            }
        } else {
            if (manager.addWorker(building)) {
                ShowMessage.showMessage("Vous avez bien ajouté 1 travailleur.",controller);
            } else {
                ShowMessage.showMessage("Ce batiment ne peut contenir de travailleurs.",controller);
            }
        }
    }

    /**
     * Permet de choisir parmis tous les types de batiment, 1 batiment à construire.
     */
    public void buildingSelectionChoice() {
        int i;
        int selection = -1;
        BuildingType[] buildingTypes = BuildingType.values();
        while(selection <= -1 || selection >= buildingTypes.length) {
            i=0;
            for (BuildingType buildingType : buildingTypes) {
                BuildingBuilder builder = gameSettings.getBuilderFromBuilding(buildingType);
                ShowMessage.showMessage("tappez " + i + " pour acheter "+buildingType+": "+builder.toString(),controller);
                i++;
            }
            Scanner reader = new Scanner(System.in);
            selection = Integer.parseInt(reader.next());
        }
        buildingSelection(buildingTypes[selection]);
    }

    /**
     * Si manager.buyBuilding(type) renvoie vrai (donc assez de ressources), on commence à construire le batiment
     * @param buildingType
     */
    public void buildingSelection(BuildingType buildingType) {

        if(manager.buyBuilding(buildingType)) {
            ShowMessage.showMessage("Vous avez bien commencé à construire: "+buildingType,controller);
        } else {
            ShowMessage.showMessage("Vous n'avez pas assez d'argent / ressources",controller);
        }
    }
}
