package application.game;

import domain.entities.buildings.*;
import domain.entities.buildings.builder.*;
import domain.entities.resources.ResourceType;
import domain.MaterialsList;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Classe qui permet de gérer toute la logique de chargement de la partie. Elle lit le fichier de resource "settings.yml"
 * et crée les builders et autres outils qui seront utiles au manager tout au long de la partie.
 */
public class GameSettings {

    //On laisse un accès privé, on peut les récupérer que par le getter. Application manager est la seule classe à
    // pouvoir y accéder étant donné que c'est elle qui gère la logique de création.
    // La sauvegarde sera faite dans la logique d'infrastructure.
    private ApartmentBuildingBuilder apartmentBuildingBuilder;
    private CementPlantBuilder cementPlantBuilder;
    private FarmBuilder farmBuilder;
    private HouseBuilder houseBuilder;
    private LumberMillBuilder lumberMillBuilder;
    private QuarryBuilder quarryBuilder;
    private SteelMillBuilder steelMillBuilder;
    private ToolFactoryBuilder toolFactoryBuilder;
    private WoodenCabinBuilder woodenCabinBuilder;

    public GameSettings(String gameConfigurationFile) {
        loadGameConfigurationFile(gameConfigurationFile);
    }

    /**
     * Méthode utile pour des usages spécifique, par exemple obtenir le coût d'un batiment que l'on va construire
     * en utilisant les paramètres disponibles dans le builder.
     * @param building
     * @return builder : Le builder retourné correspond au type de building passé en paramètre.
     */
    public BuildingBuilder getBuilderFromBuilding(BuildingType building) {
        if(building == BuildingType.APARTMENT_BUILDING) {
            return apartmentBuildingBuilder;
        } else if(building == BuildingType.CEMENT_PLANT) {
            return cementPlantBuilder;
        } else if(building == BuildingType.FARM) {
            return farmBuilder;
        }else if(building == BuildingType.HOUSE) {
            return houseBuilder;
        }else if(building == BuildingType.LUMBER_MILL) {
            return lumberMillBuilder;
        }else if(building == BuildingType.QUARRY) {
            return quarryBuilder;
        }else if(building == BuildingType.STEEL_MILL) {
            return steelMillBuilder;
        }else if(building == BuildingType.TOOL_FACTORY) {
            return toolFactoryBuilder;
        }else if(building == BuildingType.WOODEN_CABIN) {
            return woodenCabinBuilder;
        }
        return null;
    }

    public ApartmentBuildingBuilder getApartmentBuildingBuilder() {
        return apartmentBuildingBuilder;
    }

    public CementPlantBuilder getCementPlantBuilder() {
        return cementPlantBuilder;
    }

    public FarmBuilder getFarmBuilder() {
        return farmBuilder;
    }

    public HouseBuilder getHouseBuilder() {
        return houseBuilder;
    }

    public LumberMillBuilder getLumberMillBuilder() {
        return lumberMillBuilder;
    }

    public QuarryBuilder getQuarryBuilder() {
        return quarryBuilder;
    }

    public SteelMillBuilder getSteelMillBuilder() {
        return steelMillBuilder;
    }

    public ToolFactoryBuilder getToolFactoryBuilder() {
        return toolFactoryBuilder;
    }

    public WoodenCabinBuilder getWoodenCabinBuilder() {
        return woodenCabinBuilder;
    }

    /**
     * Méthode interne qui va créer tous les builders en lisant le fichier des paramètres.
     * @param gameConfigurationFile
     */
    private void loadGameConfigurationFile(String gameConfigurationFile) {
        Yaml yaml = new Yaml();
        InputStream yamlConfigFileStream = this.getClass().getClassLoader().getResourceAsStream(gameConfigurationFile);
        Map<String, Object> yamlGameConfiguration = yaml.load(yamlConfigFileStream);

        BuildingBuilder buildingBuilder = null;

        for(String building : yamlGameConfiguration.keySet()) {
            if(building.equalsIgnoreCase("ApartmentBuilding")) {
                buildingBuilder = new ApartmentBuildingBuilder();
                apartmentBuildingBuilder = (ApartmentBuildingBuilder) buildingBuilder;
            } else if(building.equalsIgnoreCase("CementPlant")) {
                buildingBuilder = new CementPlantBuilder();
                cementPlantBuilder = (CementPlantBuilder) buildingBuilder;
            }else if(building.equalsIgnoreCase("Farm")) {
                buildingBuilder = new FarmBuilder();
                farmBuilder = (FarmBuilder) buildingBuilder;
            }else if(building.equalsIgnoreCase("House")) {
                buildingBuilder = new HouseBuilder();
                houseBuilder = (HouseBuilder) buildingBuilder;
            }else if(building.equalsIgnoreCase("LumberMill")) {
                buildingBuilder = new LumberMillBuilder();
                lumberMillBuilder = (LumberMillBuilder) buildingBuilder;
            }else if(building.equalsIgnoreCase("Quarry")) {
                buildingBuilder = new QuarryBuilder();
                quarryBuilder = (QuarryBuilder) buildingBuilder;
            }else if(building.equalsIgnoreCase("SteelMill")) {
                buildingBuilder = new SteelMillBuilder();
                steelMillBuilder = (SteelMillBuilder) buildingBuilder;
            }else if(building.equalsIgnoreCase("ToolFactory")) {
                buildingBuilder = new ToolFactoryBuilder();
                toolFactoryBuilder = (ToolFactoryBuilder) buildingBuilder;
            }else if(building.equalsIgnoreCase("WoodenCabin")) {
                buildingBuilder = new WoodenCabinBuilder();
                woodenCabinBuilder = (WoodenCabinBuilder)  buildingBuilder;
            }
            LinkedHashMap buildingSettings = (LinkedHashMap) yamlGameConfiguration.get(building);
            for(String attribute : (Set<String>) buildingSettings.keySet()) {
                if(attribute.equalsIgnoreCase("residentsNumber")) {
                    int residentsNumber = (int) buildingSettings.get(attribute);
                    buildingBuilder.setResidentsNumber(residentsNumber);
                } else if(attribute.equalsIgnoreCase("workersNumber")) {
                    int workersNumber = (int) buildingSettings.get(attribute);
                    buildingBuilder.setWorkersNumber(workersNumber);
                } else if(attribute.equalsIgnoreCase("cost")) {
                    int cost = (int) buildingSettings.get(attribute);
                    buildingBuilder.setCost(cost);
                } else if(attribute.equalsIgnoreCase("buildingMaterials")) {
                    MaterialsList buildingMaterialsList = getMaterialsList((LinkedHashMap)buildingSettings.get(attribute));
                    buildingBuilder.setBuildingMaterials(buildingMaterialsList);
                } else if(attribute.equalsIgnoreCase("consumption")) {
                    MaterialsList consumptionMaterialsList = getMaterialsList((LinkedHashMap)buildingSettings.get(attribute));
                    buildingBuilder.setConsumption(consumptionMaterialsList);
                } else if(attribute.equalsIgnoreCase("production")) {
                    MaterialsList productionMaterialsList = getMaterialsList((LinkedHashMap)buildingSettings.get(attribute));
                    buildingBuilder.setProduction(productionMaterialsList);
                } else if(attribute.equalsIgnoreCase("buildingTime")) {
                    int buildingTime = (int) buildingSettings.get(attribute);
                    buildingBuilder.setBuildingTime(buildingTime);
                }
            }
        }
    }


    /**
     * Permet de récupèrer un objet de type MaterialsList à partir d'un noeud du fichier YML.
     * @param hashMap
     * @return
     */
    public MaterialsList getMaterialsList(LinkedHashMap<String,Integer> hashMap) {
        MaterialsList materialsList = new MaterialsList();
        for(String material : hashMap.keySet()) {
            materialsList.put(ResourceType.fromString(material),hashMap.get(material));
        }
        return materialsList;
    }
}
