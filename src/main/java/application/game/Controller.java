package application.game;

import application.game.GameSettings;
import application.game.Manager;
import application.game.PlayerController;
import domain.entities.buildings.Building;
import domain.entities.buildings.BuildingType;
import domain.entities.buildings.LivingBuilding;
import domain.entities.buildings.WorkingBuilding;
import domain.entities.resources.ResourceType;
import infrastructure.GameInfrastructure;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import ui.javafx.DraggableMaker;

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;


/**
 * Interface graphique du jeu. Elle communique avec Manager, PlayerController et l'infrastructure pour
 * pouvoir afficher les bons items / envoyer les différentes actions effectuées au manager/playercontroller
 */
public class Controller implements Initializable {

    private int gridSize = 50;

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private Label cementLabel;

    @FXML
    private Label costLabel;

    @FXML
    private Label coalLabel;

    @FXML
    private ChoiceBox buildingList;

    @FXML
    private Label foodLabel;

    @FXML
    private Slider tickSlider;

    @FXML
    private Tab informationsTab;

    @FXML
    private Tab buyTab;

    @FXML
    private Label consumptionLabel;

    @FXML
    private Label peopleLabel;
    @FXML
    private Label productionLabel;


    @FXML
    private Label goldLabel;

    @FXML
    private Label selectedBuildingLabel;

    @FXML
    private Label ironLabel;

    @FXML
    private Label lumberLabel;

    @FXML
    private Label steelLabel;

    @FXML
    private Label stoneLabel;

    @FXML
    private Label woodLabel;

    @FXML
    private Label toolsLabel;

    @FXML
    private Label messagesContent;

    private Building selectedBuilding;

    private DraggableMaker draggableMaker = new DraggableMaker();

    private Manager manager;
    private GameSettings gameSettings;
    private GameInfrastructure gameInfrastructure;
    private PlayerController playerController;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        gameInfrastructure = GameInfrastructure.getGameInfrastructureInstance();
        manager = Manager.getManagerInstance(gameInfrastructure,this);
        playerController = manager.getPlayerController();
        gameSettings = manager.getGameSettings();
        selectBuilding(gameInfrastructure.getBuildings().get(0));

        buildingList.setItems(FXCollections.observableArrayList(BuildingType.values()));

        buildingList.setOnAction(event -> {
            BuildingType selectedValue = (BuildingType) buildingList.getValue();
        });

        tickSlider.setOnMouseReleased(event -> {
            manager.setTimePerTick((int)tickSlider.getValue());
        });


        informationsTab.setOnSelectionChanged(e -> {
            update(false);
        });

        buildingList.setValue(buildingList.getItems().get(0));



        buildingList.setOnAction(e -> {
            updateCost();
        });

        buyTab.setOnSelectionChanged(e -> {
            updateCost();
        });
    }

    public void tick() {
        cementLabel.setText(""+gameInfrastructure.getResource(ResourceType.CEMENT));
        coalLabel.setText(""+gameInfrastructure.getResource(ResourceType.COAL));
        foodLabel.setText(""+gameInfrastructure.getResource(ResourceType.FOOD));
        goldLabel.setText(""+gameInfrastructure.getResource(ResourceType.GOLD));
        ironLabel.setText(""+gameInfrastructure.getResource(ResourceType.IRON));
        lumberLabel.setText(""+gameInfrastructure.getResource(ResourceType.LUMBER));
        steelLabel.setText(""+gameInfrastructure.getResource(ResourceType.STEEL));
        stoneLabel.setText(""+gameInfrastructure.getResource(ResourceType.STONE));
        woodLabel.setText(""+gameInfrastructure.getResource(ResourceType.WOOD));
        toolsLabel.setText(""+gameInfrastructure.getResource(ResourceType.TOOLS));
    }

    public void selectBuilding(Building building) {
        if(selectedBuilding!=null)
            selectedBuilding.getRectangle().setStroke(Color.BLACK);
        selectedBuilding = building;
        selectedBuilding.getRectangle().setStroke(Color.AQUA);
        selectedBuildingLabel.setText(building.toString());
        update(true);
    }

    public void addBuilding(Building building) {
        Platform.runLater(() -> {
            anchorPane.getChildren().add(building.getRectangle());
            draggableMaker.makeDraggable(building.getRectangle());
            building.getRectangle().setOnMouseClicked(e -> {
                selectBuilding(building);
            });
        });
    }

    public void removeBuilding(Building building) {

        anchorPane.getChildren().remove(building.getRectangle());
        selectBuilding(gameInfrastructure.getBuildings().get(0));
    }

    public void showMessage(String message) {

        messagesContent.setText(message);
    }

    @FXML
    public void buyBuilding() {
        playerController.buildingSelection((BuildingType) buildingList.getValue());
    }

    @FXML
    public void deleteBuilding() {
        playerController.removeBuilding(selectedBuilding);
    }

    public void updateCost() {

        BuildingType type =(BuildingType)buildingList.getValue();

        HashMap<ResourceType, Integer> buildingMaterials = gameSettings.getBuilderFromBuilding(type).build().getBuildingMaterials().getMaterialsList();
        int cost = gameSettings.getBuilderFromBuilding(type).getCost();
        StringBuilder costBuilder = new StringBuilder();

        for(ResourceType resourceType : buildingMaterials.keySet()) {
            costBuilder.append(resourceType+" x"+buildingMaterials.get(resourceType)+",");
        }

        costBuilder.append("GOLD x"+cost);
        costLabel.setText(costBuilder.toString());
    }

    public void update(boolean whosCalling) {
        if(!whosCalling)
            selectBuilding(selectedBuilding);
        StringBuilder consumptionBuilder = new StringBuilder("Consommation :");
        StringBuilder productionBuilder = new StringBuilder("Production :");
        int amountPeople = 0;
        if(selectedBuilding instanceof WorkingBuilding) {
            WorkingBuilding workingBuilding = (WorkingBuilding) selectedBuilding;
            for (ResourceType type : selectedBuilding.getConsumption().getMaterialsList().keySet()) {
                int quantity = selectedBuilding.getConsumption().getMaterialsList().get(type);
                if (quantity > 0) {
                    consumptionBuilder.append(ResourceType.fromResource(type) + " x" + (quantity * workingBuilding.getWorkers().size())+",");
                }
            }
            for (ResourceType type : selectedBuilding.getProduction().getMaterialsList().keySet()) {
                int quantity = selectedBuilding.getProduction().getMaterialsList().get(type);
                if (quantity > 0) {
                    productionBuilder.append(ResourceType.fromResource(type) + " x" + (quantity * workingBuilding.getWorkers().size())+",");
                }
            }
            amountPeople += workingBuilding.getWorkers().size();
        }
        if(selectedBuilding instanceof LivingBuilding){
            amountPeople += ((LivingBuilding)selectedBuilding).getResidents().size();
        }

        consumptionBuilder.append("Food x"+amountPeople);
        consumptionLabel.setText(consumptionBuilder.toString());
        productionLabel.setText(productionBuilder.toString());
        peopleLabel.setText("Personnel: "+selectedBuilding.toString());
    }

    @FXML
    public void addResident() {
        playerController.addHuman(true,selectedBuilding);
        update(false);
    }

    @FXML
    public void removeResident() {
        playerController.removeHuman(true,selectedBuilding);
        update(false);
    }

    @FXML
    public void addWorker() {
        playerController.addHuman(false,selectedBuilding);
        update(false);
    }

    @FXML
    public void removeWorker() {
        playerController.removeHuman(false,selectedBuilding);
        update(false);
    }
}