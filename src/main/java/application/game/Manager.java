package application.game;

import infrastructure.events.Event;
import domain.entities.buildings.*;
import domain.entities.buildings.builder.BuildingBuilder;
import domain.entities.human.Human;
import domain.entities.human.Resident;
import domain.entities.human.Worker;
import domain.entities.resources.*;
import infrastructure.GameInfrastructure;
import javafx.application.Platform;
import ui.PlayerConsoleGUI;
import ui.ShowMessage;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import static application.game.Manager.GameStatus.*;

public class Manager {

    private int timePerTick;
    private static Manager managerInstance;
    private GameInfrastructure gameInfrastructureInstance;
    private PlayerConsoleGUI playerConsoleGUI;
    private GameSettings gameSettings;
    private PlayerController playerController;

    private Controller controller;

    /**
     * Différents status possible du jeu, sachant que dans le mode interface graphique, on utilise que le Running et le STOPPED.
     * Le système de Pause n'a pas été implémenté, la partie se déroule donc en continue toutes les valeur définie par tick
     * (définissable et modifiable dans l'UI)
     */
    enum GameStatus {
        RUNNING, PAUSED, STOPPED;
    }

    private GameStatus status;

    /** Constructeur de type singleton pour limiter le nombre d'instance de cette classe à 1.
     * @param gameInfrastructureInstance
     */
    protected Manager(GameInfrastructure gameInfrastructureInstance) {
        init(gameInfrastructureInstance);
    }

    /**
     * Constructeur singleton mais pour la version UI. Elle ajoute en plus aux attributs de la classe, un Controller qui
     * s'occupe de gérer la partie de l'UI.
     * @param gameInfrastructureInstance
     * @param controller
     */
    protected Manager(GameInfrastructure gameInfrastructureInstance,Controller controller) {
        this.controller = controller;
        this.timePerTick = 1000;
        init(gameInfrastructureInstance);
    }

    /**
     * Méthode qui initialise l'objet manager, il lie en quelques sortes les deux constructeurs pour éviter les doublons.
     * @param gameInfrastructure
     */
    private void init(GameInfrastructure gameInfrastructure) {
        this.gameSettings = new GameSettings("settings_1.yml"); //On crée nos paramètres de jeu
        this.gameInfrastructureInstance = gameInfrastructure;
        this.playerController = new PlayerController(this,gameSettings,gameInfrastructureInstance,controller);
        this.playerConsoleGUI = new PlayerConsoleGUI(playerController);
        //Défini les resources de départ
        int STARTER_DIFFICULTY = 2;
        for(int j=0; j<STARTER_DIFFICULTY; j++) {
            WoodenCabin cabin = gameSettings.getWoodenCabinBuilder().build();
            if(controller!=null)
                controller.addBuilding(cabin);
            this.gameInfrastructureInstance.addResource(ResourceType.WOOD, 5);
            this.gameInfrastructureInstance.addResource(ResourceType.STONE, 5);
            this.gameInfrastructureInstance.addResource(ResourceType.GOLD, 30);
            this.gameInfrastructureInstance.addBuilding(cabin);
        }
        this.status = RUNNING;
        Thread gameLoop = new Thread(() -> gameLoop()); //Crée un nouveau thread qui gère toute la partie de la gameloop.
        gameLoop.start();
    }

    /**
     * Récupère l'instance de la classe. Elle crée l'objet si elle n'as pas encore été créée.
     * @param gameInfrastructureInstance
     * @return
     */
    public static Manager getManagerInstance(GameInfrastructure gameInfrastructureInstance) {
        if(managerInstance == null)
            managerInstance = new Manager(gameInfrastructureInstance);
        return managerInstance;
    }

    /**
     * Même logique mais pour l'interface graphique. (cf. constructeur)
      * @param gameInfrastructureInstance
     * @param controller
     * @return
     */
    public static Manager getManagerInstance(GameInfrastructure gameInfrastructureInstance, Controller controller) {
        if(managerInstance == null) {
            managerInstance = new Manager(gameInfrastructureInstance,controller);
        }
        return managerInstance;
    }

    public GameSettings getGameSettings() {
        return gameSettings;
    }

    public GameInfrastructure getGameInfrastructureInstance() {
        return gameInfrastructureInstance;
    }

    public GameStatus getStatus() {
        return this.status;
    }

    public PlayerController getPlayerController() {
        return this.playerController;
    }

    /**
     * Méthode executé à chaque tick. Chaque tick est lancé dans la gameloop si le temps écoulé depuis le dernier tick satisfait
     * ou non la contrainte de temps.
     */
    public void gameTick() {

        //On update en fonction des batiments présents, les resources du jeu

        ShowMessage.showMessage("[!] Début du tick", null);

        ArrayList<Human> humans = gameInfrastructureInstance.getHumans();

        //Update de la consommation / production sur les ressources du jeu
        for (Building building : gameInfrastructureInstance.getBuildings()) {
            int workersNumber = 0;
            if (building instanceof WorkingBuilding) {
                workersNumber = ((WorkingBuilding) building).getWorkers().size();
            }
            HashMap<ResourceType, Integer> consumptionHashMap = building.getConsumption().getMaterialsList();
            StringBuilder negativeMessage = new StringBuilder("[-] " + building.getName() + ": ");
            boolean negative = false;
            for (ResourceType resourceType : consumptionHashMap.keySet()) {
                int nbResources = consumptionHashMap.get(resourceType).intValue();
                if (nbResources > 0) {
                    boolean success = gameInfrastructureInstance.removeResource(resourceType, nbResources * workersNumber);
                    if (!success) {
                        status = STOPPED;
                        return;
                    }
                    negativeMessage.append(nbResources * workersNumber + " x" + resourceType + ",");
                    negative = true;
                }
            }

            HashMap<ResourceType, Integer> productionHashMap = building.getProduction().getMaterialsList();
            StringBuilder positiveMessage = new StringBuilder("[+] " + building.getName() + ": ");
            boolean positive = false;
            for (ResourceType resourceType : productionHashMap.keySet()) {
                int nbResources = productionHashMap.get(resourceType).intValue();
                if (nbResources > 0) {
                    gameInfrastructureInstance.addResource(resourceType, nbResources * workersNumber);
                    positiveMessage.append(nbResources * workersNumber + " x" + resourceType + ",");
                    positive = true;
                }
            }
            if (positive)
                ShowMessage.showMessage(positiveMessage.toString(), null);
            if (negative)
                ShowMessage.showMessage(negativeMessage.toString(), null);
        }

        int peopleToFeed = humans.size();
        if (gameInfrastructureInstance.removeResource(ResourceType.FOOD, peopleToFeed)) {
            ShowMessage.showMessage(peopleToFeed + " personnes ont été nourries", null);
        } else {
            ShowMessage.showMessage("Plus assez de nourriture, le village est mort de famine...", null);
            status = STOPPED;
            return;
        }


        ShowMessage.showMessage("Gestion des events :", null);
        //Gestion des events


        /**
         * Gère les différents évènement, et stocke ceux qui sont à supprimer une fois fini.
         */
        ArrayList<Event> gameEventsRemoved = new ArrayList<>();
        for (Event e : gameInfrastructureInstance.getGameEvent()) {
            if (e.getTickNumber() <= 0) {
                ShowMessage.showMessage(e.getEventDescriptionFinished(), controller);
                Object[] parameters = e.getParameters();
                Object object = e.getObject();
                try {
                    e.eventAccomplish().invoke(object, parameters);
                    gameEventsRemoved.add(e);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else {
                ShowMessage.showMessage(e.getEventDescriptionStillRunning(), null);
                e.decrementTick();
            }
        }
        gameInfrastructureInstance.removeGameEvent(gameEventsRemoved);
        ShowMessage.showMessage("Fin des events :", null);

        ShowMessage.showMessage("Ressources actuelles :", null);
        for (ResourceType resourceType : ResourceType.values()) {
            ShowMessage.showMessage(resourceType.toString() + " x" + gameInfrastructureInstance.getResource(resourceType), null);
        }

        //On lance de manière asynchrone le tick pour le controller de l'interface graphique, sans quoi
        //cette fonction va s'executer sur un thread différent de celui du controller et ne fonctionnera pas.
        Platform.runLater(() -> {
            controller.tick();
        });
        ShowMessage.showMessage("[!] Fin du tick", null);

        if(controller == null)
            status = PAUSED;

    }

    public void resumeGame() {
        this.status = RUNNING;
    }


    //Execute un tick si le temps écoulé satisfait la condition
    public void gameLoop() {
        Long timeSpentSinceLastTick = System.currentTimeMillis();
        while(getStatus() != STOPPED) {
            if(getStatus() != PAUSED) {
                Long currentTime = System.currentTimeMillis();
                if(currentTime-timeSpentSinceLastTick>timePerTick) {
                    timeSpentSinceLastTick = currentTime;
                    gameTick();
                }
            } else {
                if(controller == null)
                    playerConsoleGUI.playerChoices();
            }
        }
        //Fin du jeu
        ShowMessage.showMessage("Partie terminée.",controller);
    }

    public void setTimePerTick(int timePerTick) {
        this.timePerTick = timePerTick;
    }

    public boolean buyBuilding(BuildingType buildingType) {
        BuildingBuilder builder = gameSettings.getBuilderFromBuilding(buildingType);
        Building building = builder.build();
        //ressources necessaires ?
        HashMap<ResourceType,Integer> buildingMaterialsList = building.getBuildingMaterials().getMaterialsList();
        if(gameInfrastructureInstance.getResource(ResourceType.GOLD) < building.getCost())
            return false;

        for(ResourceType resourceType : buildingMaterialsList.keySet()) {
            int quantity = buildingMaterialsList.get(resourceType);
            if(gameInfrastructureInstance.getResource(resourceType) < quantity) {
                return false;
            }
        }
        gameInfrastructureInstance.payResource(building);
        try {
            Method method = Manager.class.getMethod("buildBuilding", Building.class);
            Event gameEvent = new Event(
                    building.getBuildingTime(),
                    "[BUILDING - "+building.getName()+"]",
                    method,
                    this,
                    new Building[]{building}

            );
            gameInfrastructureInstance.addGameEvent(gameEvent);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean buildBuilding(Building building) {

        //Ajout des humains
        ArrayList<Resident> residents = new ArrayList<>();
        for(int i=0; i<building.getInitialResidentsNumber(); i++) {
            residents.add(new Resident());
        }
        ArrayList<Worker> workers = new ArrayList<>();
        for(int i=0; i<building.getInitialWorkersNumber(); i++) {
            workers.add(new Worker());
        }
        //Ajout du batiment & des infos
        gameInfrastructureInstance.addBuilding(building);
        controller.addBuilding(building);
        return true;
    }

    public boolean addResident(Building building) {
        if(building instanceof LivingBuilding) {
            Resident resident = new Resident();
            ((LivingBuilding)building).addResident(resident);
            return true;
        }
        return false;
    }

    public boolean addWorker(Building building) {
        if(building instanceof WorkingBuilding) {
            Worker worker = new Worker();
            ((WorkingBuilding)building).addWorker(worker);
            return true;
        }
        return false;
    }

    public boolean removeResident(Building building) {
        if(building instanceof LivingBuilding) {
            ArrayList<Resident> residents =((LivingBuilding)building).getResidents();
            if(residents.size()>0) {
                residents.remove(0);
                return true;
            }
            return false;
        }
        return false;
    }

    public boolean removeWorker(Building building) {
        if(building instanceof WorkingBuilding) {
            ArrayList<Worker> workers =((WorkingBuilding)building).getWorkers();
            if(workers.size()>0) {
                workers.remove(0);
                return true;
            }
            return false;
        }
        return false;
    }
}
